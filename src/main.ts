import './assets/main.css'

import { createPinia } from 'pinia'
import { createApp } from 'vue'

import App from '@/App.vue'
import router from '@/router'
import afterInitRoutingGuard from '@/router/afterInitRoutingGuard'
import piniaPluginPersistedState from 'pinia-plugin-persistedstate'
import * as yup from 'yup'

const app = createApp(App)

const pinia = createPinia()
pinia.use(piniaPluginPersistedState)

app.use(pinia)
app.use(router)

afterInitRoutingGuard(router)

app.mount('#app')

// Paramètres Yup
yup.setLocale({
  mixed: {
    required: 'Champ requis'
  },
  string: {
    matches: 'Le champ ne suit le format attendu'
  }
})
