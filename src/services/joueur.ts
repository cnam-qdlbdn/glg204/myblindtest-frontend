import { ErrorWithReasons, UnknownError, type ErrorDTO } from '@/services/errors'
import type Joueur from '@/types/joueurDto'
import axiosClient from '@/utils/axios'
import { AxiosError } from 'axios'

export async function whoami(): Promise<Joueur> {
  try {
    const { data } = await axiosClient.get('/joueur')
    return data as Joueur
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        "Une erreur inattendue s'est produite lors de la récupération de votre profil, veuillez réessayer plus tard",
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response && error.response.status === 401) {
      throw new ErrorWithReasons(error.response.data as ErrorDTO)
    } else {
      throw unknownError()
    }
  }
}

export async function get(id: number): Promise<Joueur> {
  try {
    const { data } = await axiosClient.get(`/joueur/${id}`)
    return data as Joueur
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        `Une erreur inattendue s'est produite lors de la récupération du profil ${id}, veuillez réessayer plus tard`,
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response) {
      if (error.response.status === 401 || error.response.status === 404) {
        throw new ErrorWithReasons(error.response.data as ErrorDTO)
      }
    }
    throw unknownError()
  }
}
