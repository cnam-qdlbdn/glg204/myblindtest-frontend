import { ErrorWithReasons, UnknownError, type ErrorDTO } from '@/services/errors'
import Authentication from '@/types/authenticationDto'
import axiosClient from '@/utils/axios'
import { AxiosError } from 'axios'
import { plainToInstance } from 'class-transformer'

export async function login(user: string, password: string): Promise<Authentication> {
  try {
    const { data } = await axiosClient.post('/login', {
      name: user,
      password
    })
    return plainToInstance(Authentication, data as Authentication)
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        "Une erreur inattendue s'est produite lors de la connexion, veuillez réessayer plus tard",
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response && error.response.status === 401) {
      throw new ErrorWithReasons(error.response.data as ErrorDTO)
    } else {
      throw unknownError()
    }
  }
}

export async function register(user: string, password: string): Promise<Authentication> {
  try {
    const { data } = await axiosClient.post('/register', {
      name: user,
      password
    })
    return plainToInstance(Authentication, data as Authentication)
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        "Une erreur inattendue s'est produite lors de l'enregistrement, veuillez réessayer plus tard",
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response) {
      if (error.response.status === 400 || error.response.status === 401) {
        throw new ErrorWithReasons(error.response.data as ErrorDTO)
      }
    }
    throw unknownError()
  }
}

export async function refresh(): Promise<Authentication> {
  try {
    const { data } = await axiosClient.post('/refresh')
    return plainToInstance(Authentication, data as Authentication)
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        "Une erreur inattendue s'est produite lors du rafraîchissement de votre connexion, veuillez réessayer plus tard",
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response && error.response.status === 401) {
      throw new ErrorWithReasons(error.response.data as ErrorDTO)
    } else {
      throw unknownError()
    }
  }
}

export async function logout(): Promise<void> {
  try {
    await axiosClient.post('/logout')
  } catch (error) {
    const unknownError = () => new UnknownError('Erreur inattendue lors de la déconnexion', error)

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response && error.response.status === 401) {
      throw new ErrorWithReasons(error.response.data as ErrorDTO)
    } else {
      throw unknownError()
    }
  }
}
