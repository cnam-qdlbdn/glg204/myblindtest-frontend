import { Partie, type CodePartie } from '@/types/partieDto'
import axiosClient from '@/utils/axios'
import { plainToInstance } from 'class-transformer'
import { ErrorWithReasons, UnknownError, type ErrorDTO } from './errors'
import { AxiosError } from 'axios'

export async function create(): Promise<CodePartie> {
  try {
    const { data } = await axiosClient.post('/partie')
    return plainToInstance(Partie, data as Partie)
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        "Une erreur inattendue s'est produite lors de la création de la partie, veuillez réessayer plus tard",
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response) {
      if (error.response.status === 400 || error.response.status === 401) {
        throw new ErrorWithReasons(error.response.data as ErrorDTO)
      }
    }
    throw unknownError()
  }
}

export async function getByCodePartie(codePartie: string): Promise<Partie> {
  try {
    const { data } = await axiosClient.get(`/partie/${codePartie}`)
    return plainToInstance(Partie, data as Partie)
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        "Une erreur inattendue s'est produite lors de la récupération de la partie, veuillez réessayer plus tard",
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response) {
      if (error.response.status === 400 || error.response.status === 401) {
        throw new ErrorWithReasons(error.response.data as ErrorDTO)
      }
    }
    throw unknownError()
  }
}
