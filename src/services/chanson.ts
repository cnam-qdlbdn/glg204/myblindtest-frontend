import { ErrorWithReasons, UnknownError, type ErrorDTO } from '@/services/errors'
import type { ChansonDeezer } from '@/types/chansonDto'
import axiosClient from '@/utils/axios'
import { AxiosError } from 'axios'

export async function getAll(): Promise<ChansonDeezer[]> {
  try {
    const { data } = await axiosClient.get('/chanson')
    return data as ChansonDeezer[]
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        "Une erreur inattendue s'est produite lors de la récupération des chansons, veuillez réessayer plus tard",
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response && error.response.status === 401) {
      throw new ErrorWithReasons(error.response.data as ErrorDTO)
    } else {
      throw unknownError()
    }
  }
}

export async function get(id: number): Promise<ChansonDeezer> {
  try {
    const { data } = await axiosClient.get(`/chanson/${id}`)
    return data as ChansonDeezer
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        `Une erreur inattendue s'est produite lors de la récupération de la chanson ${id}, veuillez réessayer plus tard`,
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response) {
      if (error.response.status === 401 || error.response.status === 404) {
        throw new ErrorWithReasons(error.response.data as ErrorDTO)
      }
    }
    throw unknownError()
  }
}

export async function addValeurTag(idChanson: number, idTag: number, valeur: string) {
  try {
    const { data } = await axiosClient.put(`/chanson/${idChanson}/tag/${idTag}`, {
      value: valeur
    })
    return data as ChansonDeezer
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        `Une erreur inattendue s'est produite lors de l'ajout de la valeur de tag "${valeur}" pour la chanson ${idChanson} et le tag ${idTag}, veuillez réessayer plus tard`,
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response) {
      if (error.response.status === 401 || error.response.status === 404) {
        throw new ErrorWithReasons(error.response.data as ErrorDTO)
      }
    }
    throw unknownError()
  }
}

export async function deleteValeurTag(idChanson: number, idTag: number) {
  try {
    const { data } = await axiosClient.delete(`/chanson/${idChanson}/tag/${idTag}`)
    return data as ChansonDeezer
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        `Une erreur inattendue s'est produite lors de la suppression de la valeur de tag pour la chanson ${idChanson} et le tag ${idTag}, veuillez réessayer plus tard`,
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response) {
      if (error.response.status === 401 || error.response.status === 404) {
        throw new ErrorWithReasons(error.response.data as ErrorDTO)
      }
    }
    throw unknownError()
  }
}
