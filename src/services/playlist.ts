import { ErrorWithReasons, UnknownError, type ErrorDTO } from '@/services/errors'
import type DynamicPlaylist from '@/types/dynamicPlaylistDto'
import type Playlist from '@/types/playlistDto'
import axiosClient from '@/utils/axios'
import { AxiosError } from 'axios'

export async function getAll(): Promise<Playlist[]> {
  try {
    const { data } = await axiosClient.get('/playlist')
    return data as Playlist[]
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        "Une erreur inattendue s'est produite lors de la récupération des playlists, veuillez réessayer plus tard",
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response && error.response.status === 401) {
      throw new ErrorWithReasons(error.response.data as ErrorDTO)
    } else {
      throw unknownError()
    }
  }
}

export async function get(id: number): Promise<Playlist> {
  try {
    const { data } = await axiosClient.get(`/playlist/${id}`)
    return data as Playlist
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        `Une erreur inattendue s'est produite lors de la récupération de la playlist ${id}, veuillez réessayer plus tard`,
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response) {
      if (error.response.status === 401 || error.response.status === 404) {
        throw new ErrorWithReasons(error.response.data as ErrorDTO)
      }
    }
    throw unknownError()
  }
}

export async function getAllDynamic(): Promise<DynamicPlaylist[]> {
  try {
    const { data } = await axiosClient.get('/playlist/dynamic')
    return data as DynamicPlaylist[]
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        "Une erreur inattendue s'est produite lors de la récupération des playlists dynamiques, veuillez réessayer plus tard",
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response && error.response.status === 401) {
      throw new ErrorWithReasons(error.response.data as ErrorDTO)
    } else {
      throw unknownError()
    }
  }
}

export async function getDynamicById(
  idTag: number,
  valeurTag?: string
): Promise<DynamicPlaylist[]> {
  try {
    if (valeurTag) {
      const { data } = await axiosClient.get(`/playlist/dynamic/${idTag}`, {
        params: { valeurTag }
      })
      return data as DynamicPlaylist[]
    } else {
      const { data } = await axiosClient.get(`/playlist/dynamic/${idTag}`)
      return data as DynamicPlaylist[]
    }
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        "Une erreur inattendue s'est produite lors de la récupération des playlists dynamiques, veuillez réessayer plus tard",
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response && error.response.status === 401) {
      throw new ErrorWithReasons(error.response.data as ErrorDTO)
    } else {
      throw unknownError()
    }
  }
}
