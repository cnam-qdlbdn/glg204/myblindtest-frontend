import { ErrorWithReasons, UnknownError, type ErrorDTO } from '@/services/errors'
import type { CreateTag, Tag, UpdateTag } from '@/types/tagDto'
import axiosClient from '@/utils/axios'
import { AxiosError } from 'axios'

export async function getAll(): Promise<Tag[]> {
  try {
    const { data } = await axiosClient.get('/tag')
    return data as Tag[]
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        "Une erreur inattendue s'est produite lors de la récupération des tags, veuillez réessayer plus tard",
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response && error.response.status === 401) {
      throw new ErrorWithReasons(error.response.data as ErrorDTO)
    } else {
      throw unknownError()
    }
  }
}

export async function get(id: number): Promise<Tag> {
  try {
    const { data } = await axiosClient.get(`/tag/${id}`)
    return data as Tag
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        `Une erreur inattendue s'est produite lors de la récupération du tag ${id}, veuillez réessayer plus tard`,
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response) {
      if (error.response.status === 401 || error.response.status === 404) {
        throw new ErrorWithReasons(error.response.data as ErrorDTO)
      }
    }
    throw unknownError()
  }
}

export async function insert(tag: CreateTag): Promise<number> {
  try {
    const { data } = await axiosClient.post('/tag', tag)
    return (data as { id: number }).id
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        `Une erreur inattendue s'est produite lors de la création du tag ${tag.nom}, veuillez réessayer plus tard`,
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response) {
      if (error.response.status === 401) {
        throw new ErrorWithReasons(error.response.data as ErrorDTO)
      }
    }
    throw unknownError()
  }
}

export async function update(id: number, tag: UpdateTag): Promise<Tag> {
  try {
    const { data } = await axiosClient.patch(`/tag/${id}`, tag)
    return data as Tag
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        `Une erreur inattendue s'est produite lors de la mise à jour du tag ${id}, veuillez réessayer plus tard`,
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response) {
      if (error.response.status === 401 || error.response.status === 404) {
        throw new ErrorWithReasons(error.response.data as ErrorDTO)
      }
    }
    throw unknownError()
  }
}

export async function deleteTag(id: number): Promise<void> {
  try {
    await axiosClient.delete(`/tag/${id}`)
  } catch (error) {
    const unknownError = () =>
      new UnknownError(
        `Une erreur inattendue s'est produite lors de la suppression du tag ${id}, veuillez réessayer plus tard`,
        error
      )

    if (!(error instanceof AxiosError)) throw unknownError()

    if (error.response) {
      if (error.response.status === 404) {
        throw new ErrorWithReasons(error.response.data as ErrorDTO)
      }
    }
    throw unknownError()
  }
}
