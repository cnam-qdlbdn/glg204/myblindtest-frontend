export interface MyBlindTestError {
  error: ErrorDTO
}

export class ErrorWithReasons extends Error implements MyBlindTestError {
  constructor(public error: ErrorDTO) {
    super(error.reasons[0]) // fonctionne la plupart du temps
  }
}

export class UnknownError extends Error {
  constructor(message: string, public error: any) {
    super(message)
  }
}

export interface ErrorDTO {
  reasons: string[]
}
