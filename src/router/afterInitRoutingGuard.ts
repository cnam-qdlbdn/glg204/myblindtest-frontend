import { useAuthStore } from '@/stores/auth'
import { useChansonStore } from '@/stores/chanson'
import { useGlobalStore } from '@/stores/global'
import { useJoueurStore } from '@/stores/joueur'
import { usePlaylistStore } from '@/stores/playlist'
import { useTagStore } from '@/stores/tag'
import { DateTime, Duration } from 'luxon'
import type { Router } from 'vue-router'

const MAX_COOKIE_DURATION = Duration.fromObject({ hours: 2 })
const REFRESH_COOKIE_IN = Duration.fromObject({ minutes: MAX_COOKIE_DURATION.as('minutes') / 10 })

export default function afterInitRoutingGuard(router: Router) {
  router.beforeEach(async (to, from, next) => {
    // Gestion du refresh de l'authentification et du store
    const authStore = useAuthStore()
    const authInfos = authStore.authInfos
    const now = DateTime.now()

    const globalStore = useGlobalStore()
    const joueurStore = useJoueurStore()
    const playlistStore = usePlaylistStore()
    const tagStore = useTagStore()
    const chansonStore = useChansonStore()

    // on ne gère le cookie de session et l'état du store que pour les pages authentifiées.
    // Si la page ne nécessite pas d'authentification, le store peut rester en l'état, de toute
    // façon le nettoyage s'opèrera dès qu'une navigation vers une page authentifiée s'effectuera.
    if (!('allowAnonymous' in to.meta) || !to.meta.allowAnonymous) {
      if (authInfos) {
        if (authInfos.dateFinAsDate < now) {
          console.log('Navigation non authentifiée, retour vers la page de login')

          globalStore.reset()

          next({
            path: '/login',
            query: { redirect: to.fullPath }
          })
        } else {
          const expiresIn = authInfos.dateFinAsDate.diff(now, 'minutes')

          // il faut refresh bientôt, on refresh
          if (expiresIn < REFRESH_COOKIE_IN) {
            console.log('Expiration du cookie dans ', expiresIn, ', refresh du cookie')
            await authStore.refresh()
          }
          // le store est alimenté ? Si non, on l'alimente
          if (!globalStore.loaded) {
            await joueurStore.loadMyself()
            await playlistStore.loadAll()
            await playlistStore.loadAllDynamic()
            await tagStore.loadAll()
            await chansonStore.loadAll()

            globalStore.loaded = true
          }

          if (joueurStore.currentJoueur!!.codePartieParticipant && to.path !== '/partie') {
            console.log('on rejoint la partie en cours !')
            console.log('from', from)
            console.log('to', to)

            next({
              path: '/partie'
            })
          } else {
            console.log('on poursuit la navigation')

            next()
          }
        }
      } else {
        console.log('authInfos vide ?')
        next()
      }
    } else {
      // Doit autoriser la navigation si la page ne requiert pas d'authentification
      next()
    }
  })
}
