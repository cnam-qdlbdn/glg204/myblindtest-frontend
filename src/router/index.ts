import ChansonsView from '@/views/ChansonsView.vue'
import HomeView from '@/views/HomeView.vue'
import LoginView from '@/views/LoginView.vue'
import PartieJoinView from '@/views/PartieJoinView.vue'
import PartieView from '@/views/PartieView.vue'
import PlaylistsView from '@/views/PlaylistsView.vue'
import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta: {
        allowAnonymous: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView,
      props: {
        type: 'login'
      },
      meta: {
        allowAnonymous: true
      }
    },
    {
      path: '/register',
      name: 'register',
      component: LoginView,
      props: {
        type: 'register'
      },
      meta: {
        allowAnonymous: true
      }
    },
    {
      path: '/playlists',
      name: 'playlists',
      component: PlaylistsView
    },
    {
      path: '/chansons',
      name: 'chansons',
      component: ChansonsView
    },
    {
      path: '/partie/join',
      component: PartieJoinView
    },
    {
      path: '/partie',
      component: PartieView
    }
  ]
})

export default router
