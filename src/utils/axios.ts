import axios from 'axios'
import type { CreateAxiosDefaults } from 'axios'
import { DateTime } from 'luxon'

const axiosClient = axios.create(<CreateAxiosDefaults<any>>{
  withCredentials: true,
  baseURL: import.meta.env.VITE_MBT_BACKEND_URL
})

// thanks to https://stackoverflow.com/a/66238542
function handleDates(body: any) {
  if (body === null || body === undefined || typeof body !== 'object') return body

  for (const key of Object.keys(body)) {
    const value = body[key]
    if (
      value &&
      typeof value === 'string' &&
      /^\d{4}(?:-\d{2}){2}T(?:\d{2}:){2}\d{2}\.\d{6}$/.test(value)
    ) {
      body[key] = DateTime.fromISO(value)
    } else if (typeof value === 'object') {
      handleDates(value)
    }
  }
}

axiosClient.interceptors.response.use((originalResponse) => {
  // handleDates(originalResponse);
  return originalResponse
})

export default axiosClient
