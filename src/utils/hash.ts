export function hash(str: string) {
  if (!str) {
    return 0
  }

  let hash = 0
  for (let i = 0; i < str.length; i++) {
    const code = str.charCodeAt(i)
    hash = (hash << 5) - hash + code
    hash = hash & hash
  }

  return hash
}

export function hashs(params: any[]) {
  if (!params) {
    return 0
  }

  let h = 1
  for (const param of params) {
    h = 31 * h
    if (param === null || typeof param === 'undefined') {
      h += 0
    } else if (typeof param === 'number') {
      h += param
    } else if (typeof param === 'string') {
      h += hash(param)
    } else {
      throw new Error('Unsupported type : ' + param)
    }
  }

  return h
}
