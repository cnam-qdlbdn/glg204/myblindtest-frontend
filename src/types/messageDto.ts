import type { ChansonDeezer } from './chansonDto'
import type { HistoriquePartie } from './historiquePartieDto'
import type Joueur from './joueurDto'
import { Partie, type UpdatePartie } from './partieDto'
import type ResultatManche from './resultatMancheDto'

export enum TypeMessage {
  CONNECT = 'CONNECT', // out
  CONNECT_BROADCAST = 'CONNECT_BROADCAST', // in
  DISCONNECT_BROADCAST = 'DISCONNECT_BROADCAST', // in // pas de besoin de message "out" DISCONNECT, il suffira de fermer la connexion pour prévenir tout le monde
  UPDATE_ROOM_OPTIONS = 'UPDATE_ROOM_OPTIONS', // out
  UPDATE_ROOM_OPTIONS_ERROR = 'UPDATE_ROOM_OPTIONS_ERROR', // in
  UPDATE_ROOM_OPTIONS_BROADCAST = 'UPDATE_ROOM_OPTIONS_BROADCAST', // in
  INITIAL_GAME_INFORMATIONS = 'INITIAL_GAME_INFORMATIONS', // in
  EVERYONE_OUT = 'EVERYONE_OUT', // in
  START_GAME = 'START_GAME', // out // équivalent à NEXT_SONG, et permet aussi de démarrer la partie
  START_GAME_BROADCAST = 'START_GAME_BROADCAST', // in
  START_GAME_TO_GAMEMASTER = 'START_GAME_TO_GAMEMASTER', // in

  EXIT = 'EXIT', // out
  EXIT_BROADCAST = 'EXIT_BROADCAST', // in
  PLAYER_READY = 'PLAYER_READY', // out
  PLAYER_READY_BROADCAST = 'PLAYER_READY_BROADCAST', // in
  START_SONG = 'START_SONG', // in
  BUZZER_TRIGGERED = 'BUZZER_TRIGGERED', // out
  BUZZER_TRIGGERED_BROADCAST = 'BUZZER_TRIGGERED_BROADCAST', // in
  ANSWER = 'ANSWER', // out
  ANSWER_TO_GAMEMASTER = 'ANSWER_TO_GAMEMASTER', // in (specific)
  ANSWER_OK_FROM_GAMEMASTER = 'ANSWER_OK_FROM_GAMEMASTER', // out
  ANSWER_KO_FROM_GAMEMASTER = 'ANSWER_KO_FROM_GAMEMASTER', // out
  ANSWER_OK_BROADCAST = 'ANSWER_OK_BROADCAST', // in
  ANSWER_KO_BROADCAST = 'ANSWER_KO_BROADCAST', // in
  SONG_ENDED = 'SONG_ENDED', // out
  NEXT_SONG = 'NEXT_SONG', // in
  NEXT_SONG_TO_GAMEMASTER = 'NEXT_SONG_TO_GAMEMASTER', // in
  PLEASE_WAIT_FOR_NEXT_SONG = 'PLEASE_WAIT_FOR_NEXT_SONG', // in // dans le cas où un joueur vient de se reconnecter, il faut qu'il attende la prochaine chanson
  GAME_FINISHED = 'GAME_FINISHED' // in
}

export interface MessageDTO {
  type: TypeMessage
  content: MessageContent
}

export interface MessageContent { }

export class Connect implements MessageDTO {
  public type = TypeMessage.CONNECT
  public content: { codePartie: string }

  constructor(codePartie: string) {
    this.content = { codePartie }
  }
}
export class ConnectBroadcast implements MessageDTO {
  public type = TypeMessage.CONNECT_BROADCAST
  public content: { refJoueur: number }

  constructor(refJoueur: number) {
    this.content = { refJoueur }
  }
}
export class DisconnectBroadcast implements MessageDTO {
  public type = TypeMessage.DISCONNECT_BROADCAST
  public content: { refJoueur: number }

  constructor(refJoueur: number) {
    this.content = { refJoueur }
  }
}
export class UpdateRoomOptions implements MessageDTO {
  public type = TypeMessage.UPDATE_ROOM_OPTIONS
  public content: { updatePartie: UpdatePartie }

  constructor(updatePartie: UpdatePartie) {
    this.content = { updatePartie }
  }
}
export class UpdateRoomOptionsError implements MessageDTO {
  public type = TypeMessage.UPDATE_ROOM_OPTIONS
  public content: { message: String }

  constructor(message: String) {
    this.content = { message }
  }
}
export class UpdateRoomOptionsBroadcast implements MessageDTO {
  public type = TypeMessage.UPDATE_ROOM_OPTIONS_BROADCAST
  public content: { partie: Partie }

  constructor(partie: Partie) {
    this.content = { partie }
  }
}
export class InitialGameInformations implements MessageDTO {
  public type = TypeMessage.INITIAL_GAME_INFORMATIONS
  public content: { partie: Partie }

  constructor(partie: Partie) {
    this.content = { partie }
  }
}
export class StartGame implements MessageDTO {
  public type = TypeMessage.START_GAME
  public content = {}
}
export class StartGameBroadcast implements MessageDTO {
  public type = TypeMessage.START_GAME_BROADCAST
  public content: { partie: Partie; chansonUrl: { url: string } }

  constructor(partie: Partie, chansonUrl: string) {
    this.content = { partie, chansonUrl: { url: chansonUrl } }
  }
}
export class StartGameToGameMaster implements MessageDTO {
  public type = TypeMessage.START_GAME_TO_GAMEMASTER
  public content: { partie: Partie; chanson: ChansonDeezer }

  constructor(partie: Partie, chanson: ChansonDeezer) {
    this.content = { partie, chanson }
  }
}
export class PlayerReady implements MessageDTO {
  public type = TypeMessage.PLAYER_READY
  public content = {}
}
export class BuzzerTriggered implements MessageDTO {
  public type = TypeMessage.BUZZER_TRIGGERED
  public content = {}
}
export class BuzzerTriggeredBroadcast implements MessageDTO {
  public type = TypeMessage.BUZZER_TRIGGERED_BROADCAST
  public content: { refJoueur: number }

  constructor(refJoueur: number) {
    this.content = { refJoueur }
  }
}
export class AnswerOkFromGamemaster implements MessageDTO {
  public type = TypeMessage.ANSWER_OK_FROM_GAMEMASTER
  public content = {}
}
export class AnswerOkBroadcast implements MessageDTO {
  public type = TypeMessage.ANSWER_OK_BROADCAST
  public content: { resultatManche: ResultatManche }

  constructor(resultatManche: ResultatManche) {
    this.content = { resultatManche }
  }
}
export class AnswerKoFromGamemaster implements MessageDTO {
  public type = TypeMessage.ANSWER_KO_FROM_GAMEMASTER
  public content = {}
}
export class AnswerKoBroadcast implements MessageDTO {
  public type = TypeMessage.ANSWER_KO_BROADCAST
  public content: { alreadyBuzzed: number[] }

  constructor(alreadyBuzzed: number[]) {
    this.content = { alreadyBuzzed }
  }
}
export class SongEnded implements MessageDTO {
  public type = TypeMessage.SONG_ENDED
  public content = {}
}
export class NextSongBroadcast implements MessageDTO {
  public type = TypeMessage.NEXT_SONG
  public content: { partie: Partie; chansonUrl: { url: string } }

  constructor(partie: Partie, chansonUrl: string) {
    this.content = { partie, chansonUrl: { url: chansonUrl } }
  }
}
export class NextSongToGameMaster implements MessageDTO {
  public type = TypeMessage.NEXT_SONG_TO_GAMEMASTER
  public content: { partie: Partie; chanson: ChansonDeezer }

  constructor(partie: Partie, chanson: ChansonDeezer) {
    this.content = { partie, chanson }
  }
}
export class GameFinished implements MessageDTO {
  public type = TypeMessage.GAME_FINISHED
  public content: { historiquesPartie: HistoriquePartie[] }

  constructor(historiquesPartie: HistoriquePartie[]) {
    this.content = { historiquesPartie }
  }
}