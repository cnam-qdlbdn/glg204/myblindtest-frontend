export interface ChansonDeezer {
  id: number
  titre: string
  artiste: Artiste
  valeursTags: ValeurTag[]
  idDeezer: number
  extrait: string
  image: string
  playlists: number[]
}

export interface Artiste {
  nom: string
}

export interface ValeurTag {
  tag: number
  valeur: string
}
