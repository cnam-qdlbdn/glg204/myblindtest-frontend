export default interface DynamicPlaylist {
  idTag: number
  valeurTag?: string
  chansons: number[]
}
