import * as yup from 'yup'

export default interface FormFields {
  fields: {
    label: string
    name: string
    as: 'input' | 'select'
    type?: 'password'
    rules?: yup.Schema
    children?: {
      tag: 'option'
      value: string | number
      text: string
    }[]
  }[]
}
