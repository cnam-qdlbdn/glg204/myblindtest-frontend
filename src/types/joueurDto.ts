import type { HistoriquePartie } from "./historiquePartieDto"

export default interface Joueur {
  id: number
  login: string
  pseudo?: string
  codePartieParticipant?: string
  historiquesParties: HistoriquePartie[]
}
