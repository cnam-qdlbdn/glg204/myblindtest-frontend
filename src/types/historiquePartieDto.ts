export interface HistoriquePartie {
    refJoueur: number
    nomPlaylist: string
    score: number
    maitreJoueur: boolean
}