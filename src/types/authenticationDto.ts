import { DateTime } from 'luxon'

export default class Authentication {
  constructor(public joueur: number, public dateDebut: string, public dateFin: string) {}

  get dateDebutAsDate(): DateTime {
    return DateTime.fromISO(this.dateDebut)
  }

  get dateFinAsDate(): DateTime {
    return DateTime.fromISO(this.dateFin)
  }
}
