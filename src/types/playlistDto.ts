export default interface Playlist {
  id: number
  nom: string
  chansons: number[]
}
