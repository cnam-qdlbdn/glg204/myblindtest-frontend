export type TypeRegroupement = 'DIRECT' | 'VALEUR'

export interface CreateTag {
  nom: string
  description?: string
  typeRegroupement: TypeRegroupement
}

export interface UpdateTag {
  nom?: string
  description?: string
  typeRegroupement?: TypeRegroupement
}

export interface Tag extends CreateTag {
  id: number
}
