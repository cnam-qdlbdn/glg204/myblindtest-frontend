import { DateTime } from 'luxon'

export type EtatPartie = 'CREATED' | 'OPENED' | 'STARTED' | 'FINISHED'

export type RefPlaylist =
  | { id: number }
  | { id_tag: number; id_joueur: number; valeur_tag?: string }

export type CodePartie = { codePartie: string }

export interface TogglesOptionPartie {
  checkText: boolean
  buzzer: boolean
  modeBlitz: boolean
  checkAll: boolean
}

export interface OptionsPreset {
  name: string
  label: string
  toggles: TogglesOptionPartie
}

export const optionsPresets: Map<string, OptionsPreset> = new Map(
  [
    {
      name: 'VOCAL',
      label: 'Vocal',
      toggles: { checkText: false, buzzer: true, modeBlitz: true, checkAll: false }
    },
    {
      name: 'CASUAL_TEXT',
      label: 'Entre amis',
      toggles: { checkText: true, buzzer: true, modeBlitz: true, checkAll: false }
    },
    {
      name: 'FIND_ALL',
      label: 'Trouvez tout',
      toggles: { checkText: true, buzzer: false, modeBlitz: false, checkAll: true }
    },
    {
      name: 'FASTEST',
      label: 'Le plus rapide',
      toggles: { checkText: true, buzzer: false, modeBlitz: true, checkAll: false }
    }
  ].map((p) => [p.name, p])
)

export interface UpdatePartie {
  nombreManches?: number
  checkText?: boolean
  buzzer?: boolean
  modeBlitz?: boolean
  checkAll?: boolean
  refPlaylist?: RefPlaylist
}

export class Partie implements TogglesOptionPartie {
  constructor(
    public id: number,
    public codePartie: string,
    public nombreManches: number,
    public numeroManche: number,
    public checkText: boolean,
    public buzzer: boolean,
    public modeBlitz: boolean,
    public checkAll: boolean,
    public etat: EtatPartie,
    public refCreateur: number,
    public refPlaylist: RefPlaylist,
    public nomPlaylist: string,
    public dateCreation: string,
    public participants: Participant[]
  ) {}

  get dateCreationAsDate(): DateTime {
    return DateTime.fromISO(this.dateCreation)
  }
}

export interface Participant {
  refJoueur: number
  maitreJoueur: boolean
  connected: boolean
}
