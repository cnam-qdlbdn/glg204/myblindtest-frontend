export default interface ResultatManche {
    refJoueur: number
    score: number
}