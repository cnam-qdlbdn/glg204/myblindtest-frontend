import * as playlist from '@/services/playlist'
import type DynamicPlaylist from '@/types/dynamicPlaylistDto'
import type Playlist from '@/types/playlistDto'
import { defineStore } from 'pinia'
import { computed, ref, type Ref } from 'vue'

export const usePlaylistStore = defineStore(
  'playlist',
  () => {
    const playlists: Ref<Playlist[]> = ref([])

    const playlistsAsMap = computed(() => new Map(playlists.value.map((p) => [p.id as number, p])))

    const dynamicPlaylists: Ref<DynamicPlaylist[]> = ref([])

    function replaceById(id: number, playlist: Playlist) {
      playlists.value[playlists.value.findIndex((p) => p.id === id)] = playlist
    }

    async function loadAll() {
      const ps = await playlist.getAll()

      playlists.value = ps.sort((a, b) => a.id - b.id)
    }

    async function load(id: number) {
      const p = await playlist.get(id)

      replaceById(id, p)
    }

    async function loadAllDynamic() {
      const ps = await playlist.getAllDynamic()

      dynamicPlaylists.value = ps
    }

    function reset() {
      playlists.value = []
      dynamicPlaylists.value = []
    }

    return { playlists, playlistsAsMap, dynamicPlaylists, loadAll, load, loadAllDynamic, reset }
  },
  {
    persist: true
  }
)
