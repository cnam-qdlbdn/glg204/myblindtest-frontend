import type {
  AnswerKoBroadcast,
  AnswerOkBroadcast,
  BuzzerTriggeredBroadcast,
  ConnectBroadcast,
  DisconnectBroadcast,
  GameFinished,
  InitialGameInformations,
  NextSongBroadcast,
  NextSongToGameMaster,
  StartGameBroadcast,
  StartGameToGameMaster,
  UpdateRoomOptionsBroadcast
} from '@/types/messageDto'
import { Partie } from '@/types/partieDto'
import { defineStore } from 'pinia'
import { computed, ref, type Ref } from 'vue'
import { useJoueurStore } from './joueur'
import type { ChansonDeezer } from '@/types/chansonDto'
import type { HistoriquePartie } from '@/types/historiquePartieDto'
import type ResultatManche from '@/types/resultatMancheDto'
import { useMessageStore } from './message'

export const usePartieStore = defineStore('partie', () => {
  const joueurStore = useJoueurStore()
  const messageStore = useMessageStore()

  const _partie: Ref<Partie | null> = ref(null)
  const codePartie: Ref<string | null> = ref(null)
  const currentChansonUrl: Ref<string | null> = ref(null)
  const currentChansonData: Ref<ChansonDeezer | null> = ref(null)
  const waitForNextSong: Ref<boolean> = ref(false)
  const audioDuration: Ref<number | null> = ref(null)
  const alreadyBuzzed: Ref<number[]> = ref([])
  const buzzerActivated: Ref<boolean | null> = ref(null)
  const resultatManches: Ref<ResultatManche[]> = ref([])
  const historiquesPartie: Ref<HistoriquePartie[]> = ref([])

  const partie = computed(() => _partie.value)

  function onInitialGameInformations(message: InitialGameInformations) {
    _partie.value = message.content.partie
    _partie.value.participants.map((p) => p.refJoueur).forEach((pid) => joueurStore.load(pid))
  }

  function onConnectBroadcast(message: ConnectBroadcast) {
    const p = _partie.value!!

    if (p.etat === 'OPENED') {
      p.participants.push({
        refJoueur: message.content.refJoueur,
        connected: true,
        maitreJoueur: false
      })
      p.participants = p.participants.sort((a, b) => a.refJoueur - b.refJoueur)

      joueurStore.load(message.content.refJoueur)
    } else if (p.etat === 'STARTED') {
      const participant = p.participants.find((e) => e.refJoueur === message.content.refJoueur)!!
      participant.connected = true
    }
  }

  function onDisconnectBroadcast(message: DisconnectBroadcast) {
    const p = _partie.value!!

    if (p.etat === 'OPENED') {
      p.participants.splice(
        p.participants.findIndex((e) => e.refJoueur === message.content.refJoueur),
        1
      )
    } else if (p.etat === 'STARTED') {
      const participant = p.participants.find((e) => e.refJoueur === message.content.refJoueur)!!
      participant.connected = false
    }
  }

  function onUpdateRoomOptionsBroadcast(message: UpdateRoomOptionsBroadcast) {
    _partie.value = message.content.partie
  }

  function onEveryoneOut() {
    _partie.value = null
  }

  function onSongChanged(partie: Partie) {
    _partie.value = partie
    buzzerActivated.value = true
    alreadyBuzzed.value = []
    audioDuration.value = null
    waitForNextSong.value = false
  }

  function onSongChangedBroadcast(message: StartGameBroadcast | NextSongBroadcast) {
    onSongChanged(message.content.partie)
    currentChansonUrl.value = message.content.chansonUrl.url
  }

  function onSongChangedToGamemaster(message: StartGameToGameMaster | NextSongToGameMaster) {
    onSongChanged(message.content.partie)
    currentChansonData.value = message.content.chanson
  }

  function onPleaseWaitForNextSong() {
    waitForNextSong.value = true
  }

  function getJoueurById(id: number) {
    return joueurStore.joueurs.find(j => j.id === id)!!
  }

  function onBuzzerTriggeredBroadcast(message: BuzzerTriggeredBroadcast) {
    buzzerActivated.value = false
    alreadyBuzzed.value.push(message.content.refJoueur)

    messageStore.addMessage(getJoueurById(message.content.refJoueur).login + ' veut répondre !')
  }

  function onAnswerOkBroadcast(message: AnswerOkBroadcast) {
    buzzerActivated.value = true
    resultatManches.value.push(message.content.resultatManche)

    messageStore.addMessage('Bravo ' + getJoueurById(message.content.resultatManche.refJoueur).login + ' !')
  }

  function onAnswerKoBroadcast(message: AnswerKoBroadcast) {
    buzzerActivated.value = true
    alreadyBuzzed.value = message.content.alreadyBuzzed

    messageStore.addMessage("Ce n'était pas ça, la partie continue...")
  }

  function onGameFinished(message: GameFinished) {
    _partie.value = null
    historiquesPartie.value = message.content.historiquesPartie
  }

  function reset() {
    _partie.value = null
    codePartie.value = null
    currentChansonUrl.value = null
    currentChansonData.value = null
    waitForNextSong.value = false
    audioDuration.value = null
    alreadyBuzzed.value = []
    resultatManches.value = []
  }

  return {
    partie,
    codePartie,
    currentChansonUrl,
    currentChansonData,
    waitForNextSong,
    audioDuration,
    alreadyBuzzed,
    buzzerActivated,
    resultatManches,
    historiquesPartie,
    onInitialGameInformations,
    onConnectBroadcast,
    onDisconnectBroadcast,
    onUpdateRoomOptionsBroadcast,
    onEveryoneOut,
    onSongChangedBroadcast,
    onSongChangedToGamemaster,
    onPleaseWaitForNextSong,
    onBuzzerTriggeredBroadcast,
    onAnswerOkBroadcast,
    onAnswerKoBroadcast,
    onGameFinished,
    reset
  }
})
