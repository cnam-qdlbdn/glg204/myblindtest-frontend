import * as joueur from '@/services/joueur'
import type Joueur from '@/types/joueurDto'
import { defineStore } from 'pinia'
import { ref, type Ref } from 'vue'
import { usePartieStore } from './partie'

export const useJoueurStore = defineStore(
  'joueur',
  () => {
    const currentJoueur: Ref<Joueur | null> = ref(null)

    const joueurs: Ref<Joueur[]> = ref([])

    async function loadMyself() {
      const res = await joueur.whoami()
      currentJoueur.value = res

      if (res && res.codePartieParticipant) {
        const partieStore = usePartieStore()
        partieStore.codePartie = res.codePartieParticipant
      }

      replaceJoueur(res)
    }

    async function load(id: number) {
      if (joueurs.value.some((j) => j.id === id)) {
        return
      }

      const res = await joueur.get(id)

      replaceJoueur(res)
    }

    function reset() {
      currentJoueur.value = null
      joueurs.value = []
    }

    function replaceJoueur(joueur: Joueur) {
      const joueurIndex = joueurs.value.findIndex((j) => j.id === joueur.id)
      if (joueurIndex !== -1) {
        joueurs.value.splice(joueurIndex, 1)
      }

      joueurs.value.push(joueur)
    }

    return { currentJoueur, joueurs, loadMyself, load, reset }
  },
  {
    persist: true
  }
)
