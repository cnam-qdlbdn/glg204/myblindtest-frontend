import { max } from 'lodash'
import { defineStore } from 'pinia'
import { computed, ref, type Ref } from 'vue'

export const useMessageStore = defineStore('message', () => {
  const messages: Ref<{ id: number; message: string }[]> = ref([])

  const messagesAsStr = computed(() => messages.value)

  function addMessage(message: string) {
    const nextMessage = { id: nextId(), message }
    messages.value.push(nextMessage)

    setTimeout(() => {
      messages.value.splice(
        messages.value.findIndex((m) => m.id === nextMessage.id),
        1
      )
    }, 5000) // 5 secondes
  }

  function nextId() {
    return max(messages.value.map((m) => m.id)) ?? 1
  }

  return { messages: messagesAsStr, addMessage }
})
