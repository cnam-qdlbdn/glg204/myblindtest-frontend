import * as auth from '@/services/auth'
import Authentication from '@/types/authenticationDto'
import { plainToInstance } from 'class-transformer'
import { defineStore } from 'pinia'
import { ref, type Ref } from 'vue'

export const useAuthStore = defineStore(
  'auth',
  () => {
    const authInfos: Ref<Authentication | null> = ref(null)

    async function login(user: string, password: string) {
      const authResult = await auth.login(user, password)
      authInfos.value = authResult
    }

    async function register(user: string, password: string) {
      const authResult = await auth.register(user, password)
      authInfos.value = authResult
    }

    async function refresh() {
      const authResult = await auth.refresh()
      authInfos.value = authResult
    }

    async function logout() {
      await auth.logout()
      authInfos.value = null
    }

    return { authInfos, logout, login, register, refresh }
  },
  {
    persist: {
      serializer: {
        serialize: JSON.stringify,
        deserialize: (state) => {
          const s: { authInfos: Authentication } = JSON.parse(state)
          s.authInfos = plainToInstance(Authentication, s.authInfos)
          return s
        }
      }
    }
  }
)
