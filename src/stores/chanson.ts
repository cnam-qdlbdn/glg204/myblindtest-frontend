import * as chanson from '@/services/chanson'
import type { ChansonDeezer } from '@/types/chansonDto'
import { defineStore } from 'pinia'
import { computed, ref, type Ref } from 'vue'

export const useChansonStore = defineStore(
  'chanson',
  () => {
    const chansons: Ref<ChansonDeezer[]> = ref([])

    const chansonsAsMap = computed(() => new Map(chansons.value.map((c) => [c.id as number, c])))

    function replaceById(id: number, chanson: ChansonDeezer) {
      chansons.value[chansons.value.findIndex((c) => c.id === id)] = chanson
    }

    async function loadAll() {
      const cs = await chanson.getAll()

      chansons.value = cs.sort((a, b) => a.id - b.id)
    }

    async function load(id: number) {
      const c = await chanson.get(id)

      replaceById(id, c)
    }

    async function addValeurTag(idChanson: number, idTag: number, valeur: string) {
      const c = await chanson.addValeurTag(idChanson, idTag, valeur)

      replaceById(idChanson, c)
    }

    async function deleteValeurTag(idChanson: number, idTag: number) {
      const c = await chanson.deleteValeurTag(idChanson, idTag)

      replaceById(idChanson, c)
    }

    function reset() {
      chansons.value = []
    }

    return { chansons, chansonsAsMap, loadAll, load, addValeurTag, deleteValeurTag, reset }
  },
  {
    persist: true
  }
)
