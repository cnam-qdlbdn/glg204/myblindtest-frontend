import { defineStore } from 'pinia'
import { ref, type Ref } from 'vue'
import { useJoueurStore } from './joueur'
import { usePlaylistStore } from './playlist'
import { useChansonStore } from './chanson'
import { useTagStore } from './tag'
import { usePartieStore } from './partie'

export const useGlobalStore = defineStore('global', () => {
  const loaded: Ref<boolean> = ref(false)

  // TODO ajouter un flag pour indiquer une erreur d'opération sur les stores, qui nécessite un rechargement (global en général) (ou utiliser le paquet axios-retry)

  const joueurStore = useJoueurStore()
  const playlistStore = usePlaylistStore()
  const chansonStore = useChansonStore()
  const tagStore = useTagStore()
  const partieStore = usePartieStore()

  function reset() {
    joueurStore.reset()
    playlistStore.reset()
    chansonStore.reset()
    tagStore.reset()
    partieStore.reset()

    loaded.value = false
  }

  return { loaded, reset }
})
