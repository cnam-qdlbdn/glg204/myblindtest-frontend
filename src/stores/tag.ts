import * as tag from '@/services/tag'
import type { CreateTag, Tag, UpdateTag } from '@/types/tagDto'
import { defineStore } from 'pinia'
import { computed, ref, type Ref } from 'vue'
import { useChansonStore } from './chanson'

export const useTagStore = defineStore(
  'tag',
  () => {
    const chansonStore = useChansonStore()

    const tags: Ref<Tag[]> = ref([])

    const tagsAsMap = computed(() => new Map(tags.value.map((t) => [t.id as number, t])))

    function replaceById(id: number, tag: Tag) {
      tags.value[tags.value.findIndex((t) => t.id === id)] = tag
    }

    async function loadAll() {
      const ts = await tag.getAll()

      tags.value = ts.sort((a, b) => a.id - b.id)
    }

    async function load(id: number) {
      const t = await tag.get(id)

      replaceById(id, t)
    }

    async function insert(createTag: CreateTag) {
      const id = await tag.insert(createTag)

      await load(id)
    }

    async function update(id: number, updateTag: UpdateTag) {
      const t = await tag.update(id, updateTag)

      replaceById(id, t)
    }

    async function deleteTag(id: number) {
      await tag.deleteTag(id)
      tags.value.splice(
        tags.value.findIndex((t) => t.id === id),
        1
      )

      // on recharge toutes les chansons, car la suppression d'un tag supprime toutes les valeurs de tags associées,
      // et il est plus "simple" de recharger ainsi les chansons plutôt que retirer individuellement les valeurs de
      // tag devenues obsolètes.
      await chansonStore.loadAll()
    }

    function reset() {
      tags.value = []
    }

    return { tags, tagsAsMap, loadAll, load, insert, update, deleteTag, reset }
  },
  {
    persist: true
  }
)
